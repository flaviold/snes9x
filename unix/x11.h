#include <X11/Xlib.h>
#include <X11/extensions/XShm.h>

extern bool8 S9xMapDisplayInput (const char *n, s9xcommand_t *cmd);
struct GUIData
{
	Display			*display;
	Screen			*screen;
	Visual			*visual;
	GC				gc;
	int				screen_num;
	int				depth;
	int				pixel_format;
	int				bytes_per_pixel;
	uint32			red_shift;
	uint32			blue_shift;
	uint32			green_shift;
	uint32			red_size;
	uint32			green_size;
	uint32			blue_size;
	Window			window;
	XImage			*image;
	uint8			*snes_buffer;
	uint8			*filter_buffer;
	uint8			*blit_screen;
	uint32			blit_screen_pitch;
	bool8			need_convert;
	Cursor			point_cursor;
	Cursor			cross_hair_cursor;
	int				video_mode;
	int				mouse_x;
	int				mouse_y;
	bool8			mod1_pressed;
	bool8			no_repeat;
#ifdef MITSHM
	XShmSegmentInfo	sm_info;
	bool8			use_shared_memory;
#endif
};

static struct GUIData	GUI;
