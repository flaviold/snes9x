//#include <opencv/cv.h>
//#include <opencv/highgui.h>

#include "mkj_interface.h"

int main (int argc, char **argv)
{

	MkjInterface mkj;

	mkj.init();

	if (argc == 2){
		mkj.loadROM(argv[1]);
	} else {
		printf("\nPasse a localizacao da ROM!\n");
		return 0;
	}

	while (1)
	{
		//printf("Retorno de comando: %d\n", mkj.DoCommand("Comandando", true));
		mkj.DoCommand("Comandando", true);
		if (mkj.GameOver()){
			mkj.ResetGame();
		}
	}

	return (0);
}
