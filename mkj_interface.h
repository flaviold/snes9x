/* **************************************************
 * License info
 * **************************************************
 * Mikolaj Learning Interface
 * 		Learning interface for SNES(Super Nintendo Entertainment System) games
 *
 * base emulator:
 * 		Snes9x - Portable Super Nintendo Entertainment System (TM) emulator.
 *	 		(c) Copyright 1996 - 2002  Gary Henderson (gary.henderson@ntlworld.com), Jerremy Koot (jkoot@snes9x.com)
 *	 		(c) Copyright 2002 - 2004  Matthew Kendora
 *	 		(c) Copyright 2002 - 2005  Peter Bortas (peter@bortas.org)
 *	 		(c) Copyright 2004 - 2005  Joel Yliluoma (http://iki.fi/bisqwit/)
 *	 		(c) Copyright 2001 - 2006  John Weidman (jweidman@slip.net)
 *	 		(c) Copyright 2002 - 2006  funkyass (funkyass@spam.shaw.ca), Kris Bleakley (codeviolation@hotmail.com)
 *	 		(c) Copyright 2002 - 2010  Brad Jorsch (anomie@users.sourceforge.net), Nach (n-a-c-h@users.sourceforge.net)
 *	 		(c) Copyright 2002 - 2011  zones (kasumitokoduck@yahoo.com)
 *	 		(c) Copyright 2006 - 2007  nitsuja
 *			(c) Copyright 2009 - 2011  BearOso, OV2
 *
 * inspired by: A.L.E (Arcade Learning Environment)
 *
 * **************************************************
 *
 *	mkj_interface.h
 *
 ************************************************** */

#ifndef MKJ_INTERFACE_H_
#define MKJ_INTERFACE_H_

//#include <opencv/cv.h>
//#include <opencv/highgui.h>

#include <stdlib.h>
#include <unistd.h>
#include <ctype.h>
#include <fcntl.h>
#include <dirent.h>
#include <signal.h>
#include <errno.h>
#include <string.h>
#include <sched.h>
#include <pthread.h>
#include <sys/stat.h>
#include <sys/time.h>
#include <sys/types.h>
#include <sys/ioctl.h>
#include <sys/soundcard.h>
#include <sys/mman.h>
#include <linux/joystick.h>

#include "mkj_interface.h"
#include "snes9x.h"
#include "memmap.h"
#include "apu/apu.h"
#include "gfx.h"
#include "snapshot.h"
#include "controls.h"
#include "cheats.h"
#include "movie.h"
#include "logger.h"
#include "display.h"
#include "conffile.h"

#include "mkj_settings.h"

static const std::string Version = "0.0.1";
static const std::string Snes9xVersion = "1.53";

class MkjInterface {
public:
	MkjInterface();
	virtual ~MkjInterface();

	bool init();
	bool init(MkjSettings settings);

	//Load ROM file
	bool loadROM(std::string romFile);
//
	//Press or Release joypad command and return the reward
	int DoCommand(std::string command_name, bool press);
//
//	//Get possible joypad commands
//	std::string* GetJoypadCommands();
//
	bool GameOver();
//
	void ResetGame();

	//Get Screen Image
	//cv::Mat GetScreenImage();

	static void Splitpath(const char *path, char *drive, char *dir, char *fname, char *ext);
	static void Makepath (char *path, const char *, const char *dir, const char *fname, const char *ext);
	static void ParsePortConfig (ConfigFile &conf, int pass);
	static void SetupDefaultKeymap (void);
	static const char *GetDirectory (enum s9x_getdirtype dirtype);
	static long log2 (long num);
	static void InitJoysticks (void);
	static void ReadJoysticks (void);
	static void InitTimer (void);
	static void * ProcessSound (void *);
	static bool8 OpenSoundDevice (void);
	static void HandlePortCommand (s9xcommand_t cmd, int16 data1, int16 data2);
	static void SyncSpeed (void);
	static const char * GetFilename (const char *ex, enum s9x_getdirtype dirtype);
	static void SoundTrigger (void);
	static void Exit (void);
	static const char * GetFilenameInc (const char *ex, enum s9x_getdirtype dirtype);
	static const char * ChooseFilename (bool8 read_only);
	static void ToggleSoundChannel (int c);
	static const char * ChooseMovieFilename (bool8 read_only);
	static bool PollButton (uint32 id, bool *pressed);
	static bool PollAxis (uint32 id, int16 *value);
	static bool PollPointer (uint32 id, int16 *x, int16 *y);
	static bool8 InitUpdate (void);
	static bool8 DeinitUpdate (int width, int height);
	static bool8 ContinueUpdate (int width, int height);
	static void AutoSaveSRAM (void);
	static const char * Basename (const char *f);
	static bool8 OpenSnapshotFile (bool8 read_only, STREAM *file);
	static void CloseSnapshotFile (STREAM file);
	static bool8 MapInput (const char *n, s9xcommand_t *cmd);
	static bool DisplayPollButton (uint32 id, bool *pressed);
	static bool DisplayPollAxis (uint32 id, int16 *value);
	static bool DisplayPollPointer (uint32 id, int16 *x, int16 *y);

private:
	bool romLoaded;
	int frame_number;
	bool8 NP_Activated;
	uint32 JoypadSkip;
	int make_snes9x_dirs(void);
	void NSRTControllerSetup (void);
	std::string initialMessage();
	void S9xInitInputDevices (void);
	static s9xcommand_t S9xGetPortCommandT (const char *n);
	static s9xcommand_t S9xGetDisplayCommandT (const char *n);
};

#endif /* MKJ_INTERFACE_H_ */
