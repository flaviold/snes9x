A função S9xDoScreenShot no arquivo screenshot.cpp cria uma variavel uint16 usando GFX.Screen. 
Logo em seguida um duplo "for" começa, aparentemente criando a tela.

O tamanho da tela passado para o S9xDoScreenshot é:
	-IPPU.RenderedScreenWidth
	-IPPU.RenderedScreenHeight

Assim como GFX, IPPU é uma variável da globals.cpp