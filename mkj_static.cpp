/*
 * mkj_static.cpp
 *
 *  Created on: Sep 8, 2016
 *      Author: flavio
 */

#include "mkj_static.h"
#include "snes9x.h"
#include "conffile.h"

//void MkjStatic::Splitpath (const char *path, char *drive, char *dir, char *fname, char *ext)
//{
//	*drive = 0;
//
//	const char	*slash = strrchr(path, SLASH_CHAR),
//				*dot   = strrchr(path, '.');
//
//	if (dot && slash && dot < slash)
//		dot = NULL;
//
//	if (!slash)
//	{
//		*dir = 0;
//
//		strcpy(fname, path);
//
//		if (dot)
//		{
//			fname[dot - path] = 0;
//			strcpy(ext, dot + 1);
//		}
//		else
//			*ext = 0;
//	}
//	else
//	{
//		strcpy(dir, path);
//		dir[slash - path] = 0;
//
//		strcpy(fname, slash + 1);
//
//		if (dot)
//		{
//			fname[dot - slash - 1] = 0;
//			strcpy(ext, dot + 1);
//		}
//		else
//			*ext = 0;
//	}
//}
//
//void MkjStatic::Makepath (char *path, const char *, const char *dir, const char *fname, const char *ext)
//{
//	if (dir && *dir)
//	{
//		strcpy(path, dir);
//		strcat(path, SLASH_STR);
//	}
//	else
//		*path = 0;
//
//	strcat(path, fname);
//
//	if (ext && *ext)
//	{
//		strcat(path, ".");
//		strcat(path, ext);
//	}
//}
//
//void MkjStatic::ParsePortConfig (ConfigFile &conf, int pass)
//{
//	s9x_base_dir                   = conf.GetStringDup("Unix::BaseDir",             default_dir);
//	snapshot_filename              = conf.GetStringDup("Unix::SnapshotFilename",    NULL);
//	play_smv_filename              = conf.GetStringDup("Unix::PlayMovieFilename",   NULL);
//	record_smv_filename            = conf.GetStringDup("Unix::RecordMovieFilename", NULL);
//
//#ifdef JOYSTICK_SUPPORT
//	unixSettings.JoystickEnabled   = conf.GetBool     ("Unix::EnableGamePad",       true);
//	js_device[0]                   = conf.GetStringDup("Unix::PadDevice1",          NULL);
//	js_device[1]                   = conf.GetStringDup("Unix::PadDevice2",          NULL);
//	js_device[2]                   = conf.GetStringDup("Unix::PadDevice3",          NULL);
//	js_device[3]                   = conf.GetStringDup("Unix::PadDevice4",          NULL);
//	js_device[4]                   = conf.GetStringDup("Unix::PadDevice5",          NULL);
//	js_device[5]                   = conf.GetStringDup("Unix::PadDevice6",          NULL);
//	js_device[6]                   = conf.GetStringDup("Unix::PadDevice7",          NULL);
//	js_device[7]                   = conf.GetStringDup("Unix::PadDevice8",          NULL);
//#endif
//
//#ifdef USE_THREADS
//	unixSettings.ThreadSound       = conf.GetBool     ("Unix::ThreadSound",         false);
//#endif
//	unixSettings.SoundBufferSize   = conf.GetUInt     ("Unix::SoundBufferSize",     100);
//	unixSettings.SoundFragmentSize = conf.GetUInt     ("Unix::SoundFragmentSize",   2048);
//	sound_device                   = conf.GetStringDup("Unix::SoundDevice",         "/dev/dsp");
//
//	keymaps.clear();
//	if (!conf.GetBool("Unix::ClearAllControls", false))
//	{
//	#if 0
//		// Using an axis to control Pseudo-pointer #1
//		keymaps.push_back(strpair_t("J00:Axis0",      "AxisToPointer 1h Var"));
//		keymaps.push_back(strpair_t("J00:Axis1",      "AxisToPointer 1v Var"));
//		keymaps.push_back(strpair_t("PseudoPointer1", "Pointer C=2 White/Black Superscope"));
//	#elif 0
//		// Using an Axis for Pseudo-buttons
//		keymaps.push_back(strpair_t("J00:Axis0",      "AxisToButtons 1/0 T=50%"));
//		keymaps.push_back(strpair_t("J00:Axis1",      "AxisToButtons 3/2 T=50%"));
//		keymaps.push_back(strpair_t("PseudoButton0",  "Joypad1 Right"));
//		keymaps.push_back(strpair_t("PseudoButton1",  "Joypad1 Left"));
//		keymaps.push_back(strpair_t("PseudoButton2",  "Joypad1 Down"));
//		keymaps.push_back(strpair_t("PseudoButton3",  "Joypad1 Up"));
//	#else
//		// Using 'Joypad# Axis'
//		keymaps.push_back(strpair_t("J00:Axis0",      "Joypad1 Axis Left/Right T=50%"));
//		keymaps.push_back(strpair_t("J00:Axis1",      "Joypad1 Axis Up/Down T=50%"));
//	#endif
//		keymaps.push_back(strpair_t("J00:B0",         "Joypad1 X"));
//		keymaps.push_back(strpair_t("J00:B1",         "Joypad1 A"));
//		keymaps.push_back(strpair_t("J00:B2",         "Joypad1 B"));
//		keymaps.push_back(strpair_t("J00:B3",         "Joypad1 Y"));
//	#if 1
//		keymaps.push_back(strpair_t("J00:B6",         "Joypad1 L"));
//	#else
//		// Show off joypad-meta
//		keymaps.push_back(strpair_t("J00:X+B6",       "JS1 Meta1"));
//		keymaps.push_back(strpair_t("J00:M1+B1",      "Joypad1 Turbo A"));
//	#endif
//		keymaps.push_back(strpair_t("J00:B7",         "Joypad1 R"));
//		keymaps.push_back(strpair_t("J00:B8",         "Joypad1 Select"));
//		keymaps.push_back(strpair_t("J00:B11",        "Joypad1 Start"));
//	}
//
//	std::string section = S9xParseDisplayConfig(conf, 1);
//
//	ConfigFile::secvec_t	sec = conf.GetSection((section + " Controls").c_str());
//	for (ConfigFile::secvec_t::iterator c = sec.begin(); c != sec.end(); c++)
//		keymaps.push_back(*c);
//}

