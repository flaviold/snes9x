#ifndef MKJ_SETTINGS_H_
#define MKJ_SETTINGS_H_

#include "port.h"

class MkjSettings {
public:
	MkjSettings();
	virtual ~MkjSettings();

	bool8 	AutoDisplayMessages;
	bool8 	BlockInvalidVRAMAccessMaster;
	int 	DumpStreamsMaxFrames;
	uint32 	FrameTimeNTSC;
	uint32 	FrameTimePAL;
	int32 	HDMATimingHack;
	uint32 	InitialInfoStringTimeout;
	bool8 	JustifierMaster;
	bool8 	MouseMaster;
	bool8 	MultiPlayer5Master;
	bool8 	SixteenBitSound;
	uint32 	SkipFrames;
	bool8 	SnapshotScreenshots;
	uint32 	SoundInputRate;
	uint32 	SoundPlaybackRate;
	bool8 	Stereo;
	bool8 	StopEmulation;
	int8 	StretchScreenshots;
	bool8 	SuperScopeMaster;
	bool8 	SupportHiRes;
	bool8 	Transparency;
	uint32 	TurboSkipFrames;
	bool8 	WrongMovieStateProtection;

	uint32 SoundFragmentSize;
	uint32 SoundBufferSize;
	bool8 JoystickEnabled;
	bool8 ThreadSound;
};

#endif /* MKJ_SETTINGS_H_ */
